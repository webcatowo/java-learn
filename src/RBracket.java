public class RBracket implements StructSymbol {
    private static final char openChar = '(';
    private static final char closeChar = ')';

    private final char value;

    RBracket(char value) throws Exception {
        if(!isValid(value)) {
            throw new Exception("Это не Brace");
        }

        this.value = value;
    }

    public static boolean isValid(char value) {
        return (openChar == value || closeChar == value);
    }

    public boolean equal(StructSymbol symbol) {
        return value == symbol.getValue();
    }

    public boolean isOpen() {
        return openChar == value;
    }

    public boolean isClose() {
        return closeChar == value;
    }

    @Override
    public char getValue() {
        return value;
    }
}
