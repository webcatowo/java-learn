import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;


public class Main {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.nextLine();

        Stack<StructSymbol> stack = new Stack<StructSymbol>();

        for (int i = 0; i < inputString.length(); i++) {
            char currentSymbol = inputString.charAt(i);

            StructSymbol currentElement;

            //TODO с этим надо что то делать но я пока не знаю как это делать на JAVA
            if (CBracket.isValid(currentSymbol)) {
                currentElement = new CBracket(currentSymbol);
            } else if (SBracket.isValid(currentSymbol)) {
                currentElement = new SBracket(currentSymbol);
            } else if (RBracket.isValid(currentSymbol)) {
                currentElement = new RBracket(currentSymbol);
            } else {
                continue;
            }

            if (stack.isEmpty()) {
                stack.push(currentElement);
                continue;
            }

            StructSymbol lastElement = stack.pop();

            if(currentElement.isOpen()) {
                stack.add(currentElement);
            }

            if(currentElement.getClass() == lastElement.getClass() && currentElement.isClose()) {
                stack.remove(lastElement);
            }
        }

        if (stack.isEmpty()) {
            System.out.print("is valid");
            System.exit(0);
        } else {
            System.out.print("is not valid");
            System.exit(1);
        }
    }
}