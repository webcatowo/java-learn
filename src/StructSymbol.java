public interface StructSymbol {
    static boolean isValid(char value) {
        return false;
    }

    boolean equal(StructSymbol symbol);

    boolean isOpen();
    boolean isClose();

    char getValue();
}
